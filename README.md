# keeling

## [Keeling curve](https://en.wikipedia.org/wiki/Keeling_curve) plot for wikipedia

Mauna Loa CO2 measurements since 1958

Multilanguage

See use on : <https://commons.wikimedia.org/wiki/File:Mauna_Loa_CO2_monthly_mean_concentration.svg>

## Polar plot

Same but in polar coordinates. See <http://r.iresmi.net/2020/01/01/mauna-loa-co2-polar-plot/>
